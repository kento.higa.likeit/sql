USE QUESTION;

SELECT
  item_id
  , item_name
  , item_price
  , category_name 
FROM
  item 
  INNER JOIN item_category 
    ON item_category.category_id = item.category_id;

SELECT category_name,item_price AS total_price FROM item GROUP BY category_id;



SELECT
 category_name,
 SUM(item_price) AS total_price
FROM
  item 
  INNER JOIN item_category 
    ON item_category.category_id = item.category_id
	GROUP BY item_category.category_id
	ORDER BY total_price DESC;
